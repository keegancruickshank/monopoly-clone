import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  // Amount accummulated in free parking
  public freeParkingAmount = 0;

  // Observable scan object. Subscription update whenever a RFID tag is scanned
  public scan = this.socket.fromEvent<string[]>('scan');

  public lastScannedProperty;
  public lastScannedPropertyIndex;
  public pendingAction;

  public notification = "Scan a Title Deed Card or Action Card"

  constructor(private socket: Socket) {}

  public setNotification(notification: string) {
    // Change in game notification
    this.notification = notification;
  }

  public resetScans() {
    // Reset all actions currently waiting
    this.setNotification("Scan a Title Deed Card or Action Card");
    this.pendingAction = undefined;
  }
}
