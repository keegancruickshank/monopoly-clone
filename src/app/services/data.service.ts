import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public cards = [
    {
      scan: "136 4 120 135 115",
      type: "property"
    },
    {
      scan: "136 4 170 135 161",
      type: "property"
    },
    {
      scan: "136 4 166 135 173",
      type: "player"
    },
    {
      scan: "136 4 158 135 149",
      type: "player"
    }
  ]

  private playersSubject: BehaviorSubject<any> =  new BehaviorSubject(
    [
      {
        scan: "136 4 166 135 173",
        token: "battleship",
        money: 1500,
        active: false,
        id: 0
      },
      {
        scan: "136 4 158 135 149",
        token: "car",
        money: 1500,
        active: false,
        id: 1
      },
      {
        scan: "38 232 154 37 115",
        token: "hat",
        money: 1500,
        active: false,
        id: 2
      },
      {
        scan: "38 232 154 37 116",
        token: "horse",
        money: 1500,
        active: false,
        id: 3
      },
      {
        scan: "38 232 154 37 117",
        token: "thimble",
        money: 1500,
        active: false,
        id: 4
      },
      {
        scan: "38 232 154 37 118",
        token: "wheelbarrow",
        money: 1500,
        active: false,
        id: 5
      }
    ]
  )
  public players: Observable<any> = this.playersSubject.asObservable();
  public currentPlayers;

  private propertiesSubject: BehaviorSubject<any> = new BehaviorSubject(
    [
      {
        name: "Old Kent Road",
        id: 0,
        buy: 60,
        rent0: 2,
        rent1: 10,
        rent2: 30,
        rent3: 90,
        rent4: 160,
        rent5: 250,
        status: "sale",
        currentRentLevel: 0,
        owner: null,
        side: 1,
        scan: "136 4 120 135 115"
      },
      {
        name: "Whitechapel Road",
        id: 1,
        buy: 60,
        rent0: 4,
        rent1: 20,
        rent2: 60,
        rent3: 180,
        rent4: 320,
        rent5: 450,
        status: "sale",
        currentRentLevel: 0,
        side: 1,
        owner: null,
        scan: "0"
      },
      {
        name: "Kings Cross Station",
        id: 2,
        buy: 200,
        status: "sale",
        side: 1,
        owner: null,
        scan: "0"
      },
      {
        name: "The Angel Islington",
        id: 3,
        buy: 100,
        rent0: 6,
        rent1: 30,
        rent2: 90,
        rent3: 270,
        rent4: 400,
        rent5: 550,
        status: "sale",
        currentRentLevel: 0,
        side: 1,
        owner: null,
        scan: "0"
      },
      {
        name: "Euston Road",
        id: 4,
        buy: 100,
        rent0: 6,
        rent1: 30,
        rent2: 90,
        rent3: 270,
        rent4: 400,
        rent5: 550,
        status: "sale",
        currentRentLevel: 0,
        side: 1,
        owner: null,
        scan: "0"
      },
      {
        name: "Pentonville Road",
        id: 5,
        buy: 120,
        rent0: 8,
        rent1: 40,
        rent2: 100,
        rent3: 300,
        rent4: 450,
        rent5: 600,
        status: "sale",
        currentRentLevel: 0,
        side: 1,
        owner: null,
        scan: "0"
      },
      {
        name: "Pall Mall",
        id: 6,
        buy: 140,
        rent0: 10,
        rent1: 50,
        rent2: 150,
        rent3: 450,
        rent4: 625,
        rent5: 750,
        status: "sale",
        currentRentLevel: 0,
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "Electric Company",
        id: 7,
        buy: 150,
        status: "sale",
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "Whitehall",
        id: 8,
        buy: 140,
        rent0: 10,
        rent1: 50,
        rent2: 150,
        rent3: 450,
        rent4: 625,
        rent5: 750,
        status: "sale",
        side: 2,
        currentRentLevel: 0,
        owner: null,
        scan: "0"
      },
      {
        name: "Northumberland Avenue",
        id: 9,
        buy: 160,
        rent0: 12,
        rent1: 60,
        rent2: 180,
        rent3: 500,
        rent4: 700,
        rent5: 900,
        status: "sale",
        currentRentLevel: 0,
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "Marylebone Station",
        id: 10,
        buy: 200,
        status: "sale",
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "Bow Street",
        id: 11,
        buy: 180,
        rent0: 14,
        rent1: 70,
        rent2: 200,
        rent3: 550,
        rent4: 750,
        rent5: 950,
        status: "sale",
        currentRentLevel: 0,
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "Marlborough Street",
        id: 12,
        buy: 180,
        rent0: 14,
        rent1: 70,
        rent2: 200,
        rent3: 550,
        rent4: 750,
        rent5: 950,
        status: "sale",
        currentRentLevel: 0,
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "Vine Street",
        id: 13,
        buy: 200,
        rent0: 16,
        rent1: 80,
        rent2: 220,
        rent3: 600,
        rent4: 800,
        rent5: 1000,
        status: "sale",
        currentRentLevel: 0,
        side: 2,
        owner: null,
        scan: "0"
      },
      {
        name: "The Strand",
        id: 14,
        buy: 220,
        rent0: 18,
        rent1: 90,
        rent2: 250,
        rent3: 700,
        rent4: 875,
        rent5: 1050,
        status: "sale",
        currentRentLevel: 0,
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Fleet Street",
        id: 15,
        buy: 220,
        rent0: 18,
        rent1: 90,
        rent2: 250,
        rent3: 700,
        rent4: 875,
        rent5: 1050,
        status: "sale",
        currentRentLevel: 0,
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Trafalgar Square",
        id: 16,
        buy: 240,
        rent0: 20,
        rent1: 100,
        rent2: 300,
        rent3: 750,
        rent4: 925,
        rent5: 1100,
        status: "sale",
        currentRentLevel: 0,
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Fenchurch St Station",
        id: 17,
        buy: 200,
        status: "sale",
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Leicester Square",
        id: 18,
        buy: 260,
        rent0: 22,
        rent1: 110,
        rent2: 330,
        rent3: 800,
        rent4: 975,
        rent5: 1150,
        status: "sale",
        currentRentLevel: 0,
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Coventry Street",
        id: 19,
        buy: 260,
        rent0: 22,
        rent1: 110,
        rent2: 330,
        rent3: 800,
        rent4: 975,
        rent5: 1150,
        status: "sale",
        currentRentLevel: 0,
        side: 3,
        owner: null,
        scan: "136 4 170 135 161"
      },
      {
        name: "Water Works",
        id: 20,
        buy: 150,
        status: "sale",
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Piccadilly",
        id: 21,
        buy: 280,
        rent0: 22,
        rent1: 120,
        rent2: 360,
        rent3: 850,
        rent4: 1025,
        rent5: 1200,
        status: "sale",
        currentRentLevel: 0,
        side: 3,
        owner: null,
        scan: "0"
      },
      {
        name: "Regent Street",
        id: 22,
        buy: 300,
        rent0: 26,
        rent1: 130,
        rent2: 390,
        rent3: 900,
        rent4: 1100,
        rent5: 1275,
        status: "sale",
        currentRentLevel: 0,
        side: 4,
        owner: null,
        scan: "0"
      },
      {
        name: "Oxford Street",
        id: 23,
        buy: 300,
        rent0: 26,
        rent1: 130,
        rent2: 390,
        rent3: 900,
        rent4: 1100,
        rent5: 1275,
        status: "sale",
        currentRentLevel: 0,
        side: 4,
        owner: null,
        scan: "0"
      },
      {
        name: "Bond Street",
        id: 24,
        buy: 320,
        rent0: 28,
        rent1: 150,
        rent2: 450,
        rent3: 1000,
        rent4: 1200,
        rent5: 1400,
        status: "sale",
        currentRentLevel: 0,
        side: 4,
        owner: null,
        scan: "0"
      },
      {
        name: "Liverpool Street Station",
        id: 25,
        buy: 200,
        status: "sale",
        side: 4,
        owner: null,
        scan: "0"
      },
      {
        name: "Park Lane",
        id: 26,
        buy: 350,
        rent0: 35,
        rent1: 175,
        rent2: 500,
        rent3: 1100,
        rent4: 1300,
        rent5: 1500,
        status: "sale",
        currentRentLevel: 0,
        side: 4,
        owner: null,
        scan: "0"
      },
      {
        name: "Mayfair",
        id: 27,
        buy: 400,
        rent0: 50,
        rent1: 200,
        rent2: 600,
        rent3: 1400,
        rent4: 1700,
        rent5: 2000,
        status: "sale",
        currentRentLevel: 0,
        side: 4,
        owner: null,
        scan: "0"
      }
    ]
  );
  public properties: Observable<any> = this.propertiesSubject.asObservable();
  public currentProperties;

  private actionsSubject: BehaviorSubject<any> = new BehaviorSubject([]);
  public actions: Observable<any> = this.actionsSubject.asObservable();
  public currentActions;

  updatePlayers(players: any) {
    this.playersSubject.next(players);
  }

  updateProperties(properties: any) {
    this.propertiesSubject.next(properties);
  }

  updateActions(actions: any) {
    this.actionsSubject.next(actions);
  }

  activePlayerCount() {
    let count = 0;
    for(let i = 0; i < this.currentPlayers.length; i++) {
      if(this.currentPlayers[i].active) {
        count += 1;
      }
    }
    if(count >= 2) {
      return false;
    } else {
      return true;
    }
  }

  constructor() {
    this.actions.subscribe(actions => {
      this.currentActions = actions;
    })
    this.properties.subscribe(properties => {
      this.currentProperties = properties;
    })
    this.players.subscribe(players => {
      this.currentPlayers = players;
    })
  }
}
