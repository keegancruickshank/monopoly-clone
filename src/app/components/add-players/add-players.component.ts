import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../../services/game.service';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-players',
  templateUrl: './add-players.component.html',
  styleUrls: ['./add-players.component.scss']
})
export class AddPlayersComponent implements OnInit {

  private addPlayerSubscription: Subscription;

  constructor(private _game: GameService, private _router: Router, private _data: DataService) {
    // Whenever an RFID card is scanned do:
    this.addPlayerSubscription = this._game.scan.subscribe(scanID => {
      // For every card in _data.cards
      for(let i = 0; i < this._data.cards.length; i++) {
        // Find the card that matches the scanned ID
        if(scanID.toString().trim() == this._data.cards[i].scan) {
          let swippedCard = this._data.cards[i]
          // Use a switch to determine the type of card that has been swipped
          if(swippedCard.type == "player") {
            // Get player from list in _data.players
            for(let j = 0; j < this._data.currentPlayers.length; j++) {
              if(this._data.currentPlayers[j].scan == this._data.cards[i].scan) {
                // Scan player card
                this._data.currentPlayers[j].active = true;
                this._data.updatePlayers(this._data.currentPlayers);
              }
            }
          }
        }
      }
    })
  }

  scanPlayerCard(index:number) {
    this._data.currentPlayers[index].active = true;
    this._data.updatePlayers(this._data.currentPlayers);
  }

  ngOnInit() {
  }

  startGame() {
    this.addPlayerSubscription.unsubscribe();
    this._router.navigate(['game']);
  }

}
