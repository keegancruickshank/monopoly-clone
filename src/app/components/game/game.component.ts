import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../../services/game.service';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  public gameOver: boolean = false;

  //////------------------TIMER CONSTANTS---------------------//////
  // Time shown when a property is scanned
  private PROPERTYTIMER = 15;
  // Simple timer for general notifications
  private SIMPLETIMER = 5;
  //////------------------TIMER CONSTANTS---------------------//////

  //////------------------COLOR CONSTANTS---------------------//////
  private WHITECOLOR = "0x777777";
  private GREENCOLOR = "0xff0000";
  private BLUECOLOR = "0x0000ff";
  private PURPLECOLOR = "0x00ffff";
  private YELLOWCOLOR = "0xffff00";
  private ORANGECOLOR = "0xa5ff00";
  private REDCOLOR = "0x00ff00";
  private BRIGHTNESS = 255;
  //////------------------COLOR CONSTANTS---------------------//////

  //Timer variables
  private timeLeft: number = 0;
  private interval;

  // Color of current status light
  public statusLight = "";

  private filterargs = {active: true};

  private scanSubscription: Subscription;

  private allowedToScan: boolean = true;

  constructor(private _game: GameService, private _data: DataService, private _router: Router) {

    // Check if this page was accessed before players were added
    this.checkGameStarted();



    if(!this._data.activePlayerCount()) {
      let that = this;

      //Set initial status Color
      this.statusLight = "status blue";

      // Whenever an RFID card is scanned do:
      this.scanSubscription = this._game.scan.subscribe(scanID => {
        console.log("SCAN")
        // Display scanned card ID to console for debugging. Inspect elemnt on PI to view console.
        if(that.allowedToScan) {
          that.allowedToScan = false;
          setTimeout(function() {
            that.allowedToScan = true;
          }, 2000);
          console.log(scanID.toString().trim());
          // For every card in _data.cards
          for(let i = 0; i < this._data.cards.length; i++) {
            // Find the card that matches the scanned ID
            if(scanID.toString().trim() == this._data.cards[i].scan) {
              console.log("Card Matched");
              let swippedCard = this._data.cards[i]
              // Use a switch to determine the type of card that has been swipped
              switch(swippedCard.type){
                case ("property"):
                  // Get property from list in _data.properties
                  for(let j = 0; j < this._data.currentProperties.length; j++){
                    console.log("Test: " + this._data.currentProperties[j].scan);
                    console.log("Against: " + scanID.toString().trim());
                    if(this._data.currentProperties[j].scan == scanID.toString().trim()){
                      // Scan property card
                      this._game.lastScannedProperty = this._data.currentProperties[j];
                      this._game.lastScannedPropertyIndex = j;
                      this.scanPropertyCard(j);
                    }
                  }
                  break;
                case ("player"):
                  // Get player from list in _data.players
                  for(let j = 0; j < this._data.currentPlayers.length; j++) {
                    if(this._data.currentPlayers[j].scan == this._data.cards[i].scan) {
                      // Scan player card
                      console.log("User: " + j)
                      this.scanPlayerCard(j)
                    }
                  }
                  break;
                case ("action"):
                  // Get action from list in _data.action
                  for(let j = 0; j < this._data.currentActions.length; j++) {
                    if(this._data.currentActions[j].scan == this._data.cards[i].scan) {
                      // Scan action card
                      this.scanActionCard(this._data.currentActions[j])
                    }
                  }
                  break;
                default:
                  console.log("This card either doesn't exist or doesn't have a value assigned.");
                  break;
              }
            }
          }
        }
      })
    }
  }

  ngOnInit() {
  }

  private checkGameStarted() {
    // Ensure game page isn't accessed with less then 2 players
    if(this._data.activePlayerCount()) {
      this._router.navigate(["add-players"]);
    } else {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "http://localhost:3000/startBoard", true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify({
          brightness: this.BRIGHTNESS
      }));
    }
  }

  public scanActionCard(action: any) {

  }

  public scanPlayerCard(playerNum: any) {
    console.log(this._game.pendingAction);
    if(this._game.pendingAction != undefined) {
      // Switch between pending action types
      switch(this._game.pendingAction) {
        // Pass Go and Collect $200
        case ("passGo"):
          this.resetScans();
          this._data.currentPlayers[playerNum].money += 200;
          this._data.updatePlayers(this._data.currentPlayers);
          this._game.pendingAction = undefined;
          break;
        // Pay $100 tax
        case ("payTax100"):
          this._data.currentPlayers[playerNum].money -= 100;
          this._game.freeParkingAmount += 100;
          this._data.updatePlayers(this._data.currentPlayers);
          this.resetScans();
          this._game.setNotification("Free parking increased to $" + this._game.freeParkingAmount);
          this.setTimer(this.SIMPLETIMER);
          this.statusLight = "status green";
          break;
        // Pay $200 tax
        case ("payTax200"):
          this._data.currentPlayers[playerNum].money -= 200;
          this._game.freeParkingAmount += 200;
          this._data.updatePlayers(this._data.currentPlayers);
          this.resetScans();
          this._game.setNotification("Free parking increased to $" + this._game.freeParkingAmount);
          this.setTimer(this.SIMPLETIMER);
          this.statusLight = "status green";
          break;
        // Collect free parking
        case ("freeParking"):
          this.resetScans();
          this._data.currentPlayers[playerNum].money += this._game.freeParkingAmount;
          this._game.freeParkingAmount = 0;
          this._data.updatePlayers(this._data.currentPlayers);
          this._game.setNotification("Free parking reset to $0");
          this.setTimer(this.SIMPLETIMER);
          this.statusLight = "status green";
          break;
        // Landed on property, But or auction
        case ("buy"):
          this.resetScans();
          if(this._game.lastScannedProperty.buy > this._data.currentPlayers[playerNum].money) {
            // Send property to auction
          } else {
            // Find property and set player to owner.
            let index = this._game.lastScannedPropertyIndex;
            this._data.currentProperties[index].owner = this._data.currentPlayers[playerNum].token;
            this._data.currentProperties[index].status = "owned";
            this._data.currentPlayers[playerNum].money -= this._data.currentProperties[index].buy;
            this._data.updatePlayers(this._data.currentPlayers);
            this._data.updateProperties(this._data.currentProperties);
            this.fullPendingScans("You bought " + this._data.currentProperties[index].name, this.SIMPLETIMER);
            this.statusLight = "status green";
          }
          break;
        case ("pay-rent"):

          console.log("User paying rent: " + this._data.currentPlayers[playerNum].token)
          this.resetScans();
          // How much rent is owed
          let rentAmount = this._game.lastScannedProperty["rent" + this._game.lastScannedProperty.currentRentLevel];
          let owedPlayer = this._game.lastScannedProperty["owner"];

          // If the player landing on the property owns it
          console.log(this._data.currentPlayers[playerNum].token + " owes " + owedPlayer);
          if(owedPlayer == this._data.currentPlayers[playerNum].token) {
            console.log("Upgrading lvl +1")
            // Update property + 1 level unless already 5
            this.upgradeProperty(1);
            this.fullPendingScans(this._game.lastScannedProperty.name + " is at level " + (this._game.lastScannedProperty.currentRentLevel), this.SIMPLETIMER);
          // If the player that lands on the property DOES NOT own it
          } else {
            // Take the players money away
            this._data.currentPlayers[playerNum].money -= rentAmount;
            // Check if that sent the player bankrupt
            if (this.isBankrupt(this._data.currentPlayers[playerNum])) {
              this.startBankruptProcess(this._data.currentPlayers[playerNum]);
            } else {
              this.fullPendingScans(this.capitalise(owedPlayer) + " recieved $" + this._game.lastScannedProperty["rent" + this._game.lastScannedProperty.currentRentLevel] + " from " + this.capitalise(this._data.currentPlayers[playerNum].token), this.SIMPLETIMER);

              this._data.updatePlayers(this._data.currentPlayers);

              // Update property + 1 level unless already 5
              this.upgradeProperty(1);
            }
          }

          this.statusLight = "status green";
      }
    // Scan with no pending action. Player gets shown funds.
    } else {
      this.resetScans();
      let capitalToken = this._data.currentPlayers[playerNum].token.charAt(0).toUpperCase() + this._data.currentPlayers[playerNum].token.slice(1);
      this.fullPendingScans(capitalToken + " has been scanned. You have $" + this._data.currentPlayers[playerNum].money, this.SIMPLETIMER);
    }
  }

  isBankrupt(player: any) {
    // If player is negative funds return true
    if (player.money < 0) {
      console.log("You are bankrupt");
      return true;
    }
  }

  valueOfAllProperties(player:any){
      let value = 0;
      for(let i = 0; i < this._data.currentProperties.length; i++){
        if(this._data.currentProperties[i].owner == player.token) {
          value += this._data.currentProperties[i].buy;
        }
      }
      return value;
  }

  startBankruptProcess(player: any) {
    // FIX: Check if player can make back money
    if(this.valueOfAllProperties(player) < player.money * -1) {
      this._game.notification = "GAME OVER";
      this.gameOver = true;
      this.calculateAllPlayerValues();
    } else {
      this._game.notification = "You have -$" + (player.money * -1) + ". Scan cards to sell.";
    }
  }

  calculateAllPlayerValues() {
    for(let i = 0; i < this._data.currentPlayers.length; i++) {
      for(let j = 0; j < this._data.currentProperties.length; j++) {
        if(this._data.currentProperties[j].owner == this._data.currentPlayers[i].token) {
          this._data.currentPlayers[i].money += this._data.currentProperties[j].buy;
          this._data.updatePlayers(this._data.currentPlayers);
        }
      }
    }
  }

  upgradeProperty(amount: number) {
    // Search for thelast scanned propert in name in _data.properties list
    for(let j = 0; j < this._data.currentProperties.length; j++) {
      // If the property is not lvl 5 yet, see if adding them upgrade amount will send it past 5. If not, add said levels
      if(this._data.currentProperties[j].name == this._game.lastScannedProperty.name && (this._game.lastScannedProperty.currentRentLevel + amount) <= 5) {
        this._data.currentProperties[j].currentRentLevel += amount;
        this._data.updateProperties(this._data.currentProperties);
      // If the amount would send over 5, Set property to 5
      } else if ((this._data.currentProperties[j].name == this._game.lastScannedProperty.name && (this._game.lastScannedProperty.currentRentLevel + amount) >= 5)) {
        this._data.currentProperties[j].currentRentLevel = 5;
        this._data.updateProperties(this._data.currentProperties);
      }
    }
  }

  scanPropertyCard(index:number) {
    this.resetScans();
    console.log(index);
    // If the property is for sale, offer to buy or auction
    if(this._data.currentProperties[index].status == "sale") {
      this._game.pendingAction = "buy";
      console.log(this._data.currentProperties[index]);
      this.fullPendingScans(this.capitalise(this._data.currentProperties[index].name) + " is avaliable. Tap bank card to buy.", this.PROPERTYTIMER);
    // If property is owned prompt for player card
    } else if (this._data.currentProperties[index].status == "owned") {
      this._game.pendingAction = "pay-rent";
      this.fullPendingScans(this.capitalise(this._data.currentProperties[index].name) + " owned by " + this.capitalise(this._data.currentProperties[index].owner) + ". Current rent: $" + this._data.currentProperties[index]["rent" + this._data.currentProperties[index].currentRentLevel] + ". Scan player card.", this.PROPERTYTIMER);
    }
  }

  // Collect from free parking
  freeParking() {
    this.resetScans();
    this._game.pendingAction = "freeParking";
    this.fullPendingScans("Free parking is currently at $" + this._game.freeParkingAmount + ". Tap your card to claim.", this.PROPERTYTIMER);
  }

  // Pay x amount of tax
  payTax(amount) {
    this.resetScans();
    this._game.pendingAction = "payTax" + amount;
    this.fullPendingScans("You owe $" + amount + " tax. Tap your card.", this.PROPERTYTIMER);
  }

  // Pass go get $200
  passGo() {
    this.resetScans();
    this._game.pendingAction = "passGo";
    this.fullPendingScans("You Have Passed Go. Scan Your Bank Card.", this.PROPERTYTIMER);
  }

  // Update board light color values
  updatePropertyLights() {
    let propertyArray: Array<string> = [];
    for(let i = 0; i< this._data.currentProperties.length; i++) {
      let level = this._data.currentProperties[i].currentRentLevel;
      if(this._data.currentProperties[i].status == "sale") {
        propertyArray.push(this.WHITECOLOR);
      } else {
        switch(level) {
          case (0):
            propertyArray.push(this.GREENCOLOR);
            break;
          case (1):
            propertyArray.push(this.BLUECOLOR);
            break;
          case (2):
            propertyArray.push(this.PURPLECOLOR);
            break;
          case (3):
            propertyArray.push(this.YELLOWCOLOR);
            break;
          case (4):
            propertyArray.push(this.ORANGECOLOR);
            break;
          case (5):
            propertyArray.push(this.REDCOLOR);
            break;
        }
      }
    }
    this.setPropertyColor(propertyArray)
  }


  setPropertyColor(propertyArray: Array<string>) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:3000/setBoardColor", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        data: propertyArray,
        brightness: this.BRIGHTNESS
    }));
  }


  //<_________TIMER FUNCTIONS_______>//
  setTimer(value) {
    this.timeLeft = value;
    this.startTimer();
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        if(this.timeLeft == 1) {
          this._game.resetScans();
          this.statusLight = "status blue";
          clearInterval(this.interval);
        }
        this.timeLeft--;
      }
    },1000)
  }

  resetTimer() {
    clearInterval(this.interval);
    this.setTimer(0);
    this.statusLight = "status blue";
    clearInterval(this.interval);
  }
  //<_________TIMER FUNCTIONS_______>//

  resetScans() {
    this.resetTimer();
    this._game.resetScans();
  }

  fullPendingScans(message: string, time: number){
    this._game.notification = message;
    this.setTimer(time);
    this.statusLight = "status yellow";
    this.updatePropertyLights();
  }

  capitalise(text: string) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  fakeScan(index:number){
    this._game.lastScannedProperty = this._data.currentProperties[index];
    this._game.lastScannedPropertyIndex = index;
    this.scanPropertyCard(index);
  }

  moneyClass(player: any) {
    if(this.gameOver) {
      let playerToken = player.token;
      let tempMoney = 0;
      let lowest = null;
      let highest = null;
      for(let i = 0; i < this._data.currentPlayers.length; i++) {
        if(this._data.currentPlayers[i].active) {
          if(this._data.currentPlayers[i].money > tempMoney) {
            highest = this._data.currentPlayers[i];
          }
        }
      }
      tempMoney = 99999999;
      for(let j = 0; j < this._data.currentPlayers.length; j++) {
        if(this._data.currentPlayers[j].active) {
          if(this._data.currentPlayers[j].money < tempMoney) {
            lowest = this._data.currentPlayers[j];
          }
        }
      }
      if(playerToken == highest.token) {
        return "money green-money";
      } else if (playerToken == lowest.token) {
        return "money red-money";
      } else {
        return "money";
      }
    } else {
      return "money";
    }
  }

  restartGame() {
    location.reload();
  }

}
