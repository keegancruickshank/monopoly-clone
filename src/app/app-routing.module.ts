import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPlayersComponent } from './components/add-players/add-players.component';
import { GameComponent } from './components/game/game.component';


const routes: Routes = [
  { path: 'game', component: GameComponent },
  { path: 'add-players', component: AddPlayersComponent },
  { path: '',
    redirectTo: '/add-players',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
